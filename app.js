//app.js
import { ToastPannel } from './component/toastTest/toastTest'
App({
    globalData: {
        userInfo: null,
        tokenSession:"",
        experiencePattern:"",//是否体验用户
        experiencePatternValue:"虚拟体验",//是否体验用户
        hasAreaNum:"",
        // globalApi:"http://192.168.4.9:8083/",//杨总
        // globalApi:"http://192.168.88.11:8080/smarthome-web-wxminiapp/",//测试地址
        // globalApi:"http://192.168.4.2:8084/smarthome-web-mini/",//java
        globalApi:"https://aiyitest.dev.linkeddr.com/smarthome-web-wxminiapp/",//线上地址
// socketUrl:"ws://192.168.4.2:8084/smarthome-web-mini/smartHome",//websocket地址
// socketUrl:"ws://127.0.0.1:9505/index.php",//
        socketUrl:"ws://aiyitest.dev.linkeddr.com/smarthome-web-wxminiapp/smartHome",//websocket地址
        authorizeData:'',//存储新增或修改授权跳转时填写数据
        detailClientCode:'',//授权详情回调当前区域clientCode



    },
    ToastPannel,
    onLaunch: function () {
        // 展示本地存储能力
        var logs = wx.getStorageSync('logs') || [];
        logs.unshift(Date.now());
        wx.setStorageSync('logs', logs);

        // 获取用户信息
        wx.getSetting({
            success: res => {
                if (res.authSetting['scope.userInfo']) {
                    // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                    wx.getUserInfo({
                        success: res => {
                            // 可以将 res 发送给后台解码出 unionId
                            this.globalData.userInfo = res.userInfo;

                            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                            // 所以此处加入 callback 以防止这种情况
                            if (this.userInfoReadyCallback) {
                                this.userInfoReadyCallback(res)
                            }
                        }
                    })
                }
            }
        })
    },
    // 弹窗
    openAlert: function (con) {
        wx.showModal({
            content: con,
            showCancel: false,
            success: function (res) {
                if (res.confirm) {
                    // console.log('用户点击确定')
                }
            }
        });
    }

});