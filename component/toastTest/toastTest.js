let _compData = {
  '_toast_.isHide': false,// 控制组件显示隐藏
  '_toast_.title': '',// 显示的内容标题
  '_toast_.msg': '',// 显示的副内容
  '_toast_.status': ''// 显示的图标名称
}
let toastPannel = {
  // toast显示的方法
  show: function(title,status,msg,url) {//msg 可能存在的副标题
    let self = this;
    let statusIco = '';
    if(status =='success'){//成功
        statusIco = 'beibosi-tishi-tongguox'
    }else if(status =='failed'){//失败
        statusIco = 'beibosi-yinle-shanchux'
    }else if(status == 'loading'){//加载中

    }
    this.setData({ '_toast_.isHide': true, '_toast_.title': title,'_toast_.status':statusIco,'_toast_.msg':msg});
    setTimeout(function(){
      self.setData({ '_toast_.isHide': false});
      if(url){
          wx.navigateBack({//禁用成功时跳转上一页
              url:url
          });
      }
    },2000)
  }
}
function ToastPannel() {
  // 拿到当前页面对象
  let pages = getCurrentPages();
  let curPage = pages[pages.length - 1];
  this.__page = curPage;
  // 小程序最新版把原型链干掉了。。。换种写法
  Object.assign(curPage, toastPannel);
  // 附加到page上，方便访问
  curPage.toastPannel = this;
  // 把组件的数据合并到页面的data对象中
  curPage.setData(_compData);
  return this;
}
module.exports = {
  ToastPannel
}