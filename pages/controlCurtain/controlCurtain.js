const app = getApp();

Page({
    data:{
        isOpen:'',//当前设备是否开启 0 开启 1 关闭
        switchValue:{
            spotId:'4300',
            spotItemId:[63,64]
        }
    },
    //处理设备信息数据-组装逻辑部分
    filterDate:function(data){
        var that = this;
        console.log(data)
        //处理更新数据
        var isOpen ='';
        var lengthO = data.bisSpotDto.length;
        var lengthI = '';
        var spotTypeO ='';
        var spotValueI = '';
        var switchValue = '';
        if(lengthO !== 0) {//判断是否存在设备信息
            for (let i = 0; i < lengthO; i++) {//组装数据
                lengthI = data.bisSpotDto[i].bisSpotItems.length;
                spotTypeO = data.bisSpotDto[i].spotType;
                console.log(spotTypeO)

                if (spotTypeO == 'C1') {//初始化开关数据
                    isOpen = data.bisSpotDto[i].spotValue;
                    switchValue = {spotId: data.bisSpotDto[i].spotId, spotItemId: []};
                    for (let f = 0; f < lengthI; f++) {
                        spotValueI = data.bisSpotDto[i].bisSpotItems[f].itemValue;
                        switchValue.spotItemId[spotValueI] = data.bisSpotDto[i].bisSpotItems[f].spotItemId;
                    }
                    console.log(switchValue)
                    that.setData({
                        isOpen: isOpen,
                        switchValue: switchValue
                    })
                }
            }
        }
    },
    onLoad:function (options) {

        // 调用应用实例的方法获取全局数据
        let app = getApp();
        // toast组件实例
        new app.ToastPannel();
        // 页面初始化 options为页面跳转所带来的参数
        var that = this;
        var deviceId = options.deviceId;
        that.setData({
            deviceId:deviceId,
        });
        wx.request({
            url: app.globalData.globalApi+"home/quipmentDetail/"+deviceId,//关闭-开启接口
            data:{
            },
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            dataType:'json',
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                var data = res.data.data;
                console.log(data);
                that.filterDate(data);
            },
            fail:function () {
            }
        })
    },
    switchFuc:function () {//开关操作
        var that = this;
        var isOpen = that.data.isOpen;//获取当前开关状态
        var spotId = that.data.switchValue.spotId;
        var spotItemId ='';
        var setValue = '';
        if (isOpen == "0"){//判断关闭开启
            isOpen = "1";
            spotItemId = that.data.switchValue.spotItemId[1];
            setValue = {
                isOpen:isOpen,
            };
        }else{
            isOpen = "0";
            spotItemId = that.data.switchValue.spotItemId[0];
            setValue = {
                isOpen:isOpen,
                modeNum:'',  //如果操作关闭成功时 关闭所有模式
                nowTem:'' //如果操作关闭成功时 关闭温度显示
            };
        }
        that.globalChangeFuc(spotId,spotItemId,'',setValue);

    },
    globalChangeFuc:function(spotId,spotItemId,value,setValue){//value为需要传入温度值或状态值是写入  setValue为传入需要设置公共字段的数据集合
        var that = this;
        var parame = '';
        if(!spotItemId){//判断 设置请求参数
            parame = spotId+'/'+value;
        }else{
            parame = spotId+'/'+spotItemId
        }
        // that.setData(setValue);
        wx.showLoading();//
        wx.request({
            url: app.globalData.globalApi+"home/operationEquipment/"+parame,//设备页面操作请求公共接口
            data:{
            },
            method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            dataType:'json',
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                console.log(res.data.data);

                that.setData(setValue);//调用接口成功时  执行传入设置数据函数
                wx.hideLoading();//
            }
        });
    },
    onShow: function () {
        // 页面显示
        var that = this;
        that.socketFuc();//连接websocket
    },
    onHide: function () {
        // 页面隐藏
        var that = this;
        that.data.task.close();//关闭当前页websocket
    },
    onUnload: function () {
        // 页面关闭
    },
    //websocket函数
    socketFuc:function() {
        var that = this;
        var socketTask = wx.connectSocket({
            url: app.globalData.socketUrl,
            success: function (res) {
                console.log(res)
            }
        });
        console.log(socketTask)
        that.setData({
            task: socketTask
        });
        // 监听链接成功
        socketTask.onOpen(function (res) {
            console.log(res);
            socketTask.send({
                data: app.globalData.tokenSession,
                success: res => {
                    console.log(res + '发送成功')
                },
                fail: err => {
                    console.log(err + '失败')
                }

            });
        });
        // 接收更新数据
        socketTask.onMessage(function (data) {
            var objData = JSON.parse(data.data);
            console.log(objData);


            var nowDeviceId = that.data.deviceId;//获取当前设备Id
            var clientData = objData.clientData;//获取更新数据列表
            var data_type = objData.data_type; //判断传入数据源类型  device_change时为设备变更
            if(data_type=='device_change'){//如果传入数据为更新设备
                var length = clientData.length;
                var deviceId = '';
                if(length!==0){//当前设备列表存在数据时候
                    for(let i=0;i<length;i++){
                        deviceId = clientData[i].deviceId;//获取设备id
                        if(deviceId == nowDeviceId){//匹配当前设备
                            that.filterDate(clientData[i])//调用数据组装逻辑
                        }
                    }
                }

            }else{//权限有变更 给出提示后跳转首页
                that.show('权限有变更','failed','','../controlIndex/controlIndex');//弹出提示信息
            }
        });
        //连接失败
        socketTask.onError(function () {
            console.log('websocket连接失败！');
        });
    },
    // backFuc:function(){//跳转操控首页
    //     console.log(333);
    //     wx.navigateTo({
    //         url: "../controlIndex/controlIndex",
    //     });
    // }
});