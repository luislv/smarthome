//操作中心-首页
const app = getApp()
Page({
    data: {
        topBlock:'1',//头部模块显隐判断 1为天气 0为监控 默认为天气
        isLoading:'',//设备列表是否加载中 1为是 0 为否
        noData:'1',//暂无数据 0 显示 1隐藏
        //天气
        weather: {
            province: '',//省
            city: '',//市
            tempNum: '',//温度
            weatherVal: '',//天气
            weatherIco: '',//天气图标
            winddirection: '',//风向
            windpower: '' //风级
        },
        //视频
        videoSrc:'../../img/111.mp4',
        //切换区域
        selectPerson: true,
        firstPerson: '',//格式 {areaName:'',clientCode:'',roleCode:''}
        selectArea: false,
        areaData: '',//格式 [{areaName: '武汉',clientCode:"",roleCode:""}]
        //左侧设备菜单
        baseArea:[{areaName: "常用", iconName: "beibosi-shouye-changyongweixuanzhongx", areaCode: "",dataIndex:'0'}],//左侧区域列表数据 格式areaName: "主卧书房", iconName: "beibosi-shouye-woshixuanzhongx", areaCode: "2021",dataIndex:i+1
        equipmentArea:'',//左侧区域列表数据 格式areaName: "主卧书房", iconName: "beibosi-shouye-woshixuanzhongx", areaCode: "2021",dataIndex:i+1
        equipmentCurrentIndex:'0',
        //图标库
        baseIco:[{
            "beibosi-wufoggy":"66f",
            "beibosi-8":"645",
            "beibosi-caozuoshibai":"6d2",
            "beibosi-qing":"635",
            "beibosi-duoyun":"673",
            "beibosi-wumai":"636",
            "beibosi-baoxue":"637",
            "beibosi-qiangshachenbao":"638",
            "beibosi-bingdongyuxue":"641",
            "beibosi-qingzhuanduoyun":"6e2",
            "beibosi-baoyu":"663",
            "beibosi-icon-test":"63e",
            "beibosi-yujiaxue":"633",
            "beibosi-jian":"639",
            "beibosi-xinxinicon":"678",
            "beibosi-_icon":"640",
            "beibosi-kongtiao-chushi":"63d",
            "beibosi-leizhenyubanbingbao":"646",
            "beibosi-icon-test1":"63f",
            "beibosi-tianqiku-fanganyi-duoyunzhuanqingtian":"713",
            "beibosi-tianqiku-fanganyi-longjuanfeng":"71f",
            "beibosi-shuaxin":"769",
            "beibosi-icontubiao":"6c1",
            "beibosi-zanwushuju":"63a",
            "beibosi-fengsu-gao":"6f0",
            "beibosi-shuaxin1":"63b",
            "beibosi-mini-bangzhux":"600",
            "beibosi-min-guanbix":"601",
            "beibosi-min-shezhix":"602",
            "beibosi-gongneng-shaixuanx":"603",
            "beibosi-jiaju-dianshix":"604",
            "beibosi-jiaju-jiankongx":"605",
            "beibosi-jiaju-kongtiaox":"606",
            "beibosi-jiaju-saodijix":"607",
            "beibosi-jiaju-touyingx":"608",
            "beibosi-kongtiao-fengsux":"609",
            "beibosi-kongtiao-fengxiangx":"60a",
            "beibosi-kongtiao-furex":"60b",
            "beibosi-kongtiao-qiehuanmoshix":"60c",
            "beibosi-kongtiao-zhilengx":"60d",
            "beibosi-liebiao-kaiguanx":"60e",
            "beibosi-liebiao-tiaojiex":"60f",
            "beibosi-liebiao-tiaojiefangxiangx":"610",
            "beibosi-moshi-dingshix":"611",
            "beibosi-moshi-jienengx":"612",
            "beibosi-moshi-likaix":"613",
            "beibosi-moshi-zhinengx":"614",
            "beibosi-shezhi-anquanrizhix":"615",
            "beibosi-shezhi-wodeshouquanx":"616",
            "beibosi-shezhi-xunitiyanx":"617",
            "beibosi-shoujihaox":"618",
            "beibosi-shouye-changyongweixuanzhongx":"619",
            "beibosi-shouye-ketingxuanzhongx":"61a",
            "beibosi-shouye-woshixuanzhongx":"61b",
            "beibosi-tishi-tongguox":"61c",
            "beibosi-yinle-bofangzhongx":"61e",
            "beibosi-yinle-shanchux":"61f",
            "beibosi-yinle-shanchuquanbux":"620",
            "beibosi-yinle-suijibofangx":"621",
            "beibosi-yinle-xunhuanzhongx":"622",
            "beibosi-icon-shidux":"61d",
            "beibosi-icon-shuizhix":"623",
            "beibosi-icon-xiaoxix":"624",
            "beibosi-icon-youjiantoux":"625",
            "beibosi-icon-xuanzhongx":"627",
            "beibosi-chuanglian-dakaix":"628",
            "beibosi-menjin-dakaix":"629",
            "beibosi-min-guanbichuanglianx":"62a",
            "beibosi-icon-zuojiantoux1":"62b",
            "beibosi-chuanglian-tingzhiguanbix":"626",
            "beibosi-menjin-guanbix":"62c",
            "beibosi-yinle-liebiaox":"62d",
            "beibosi-yinle-shangyishoux":"62e",
            "beibosi-yinle-tingzhix":"62f",
            "beibosi-yinle-bofangx":"630",
            "beibosi-yinle-xiayishoux":"631",
            "beibosi-icon-fanhuix":"632",
            "beibosi-icon-youjiantoux-copy":"714",
            "beibosi-menjin-suox":"634",
            "beibosi-shebei-chuanglian-line":"63c"
        }],
        //右侧设备列表
        isBase:'',//是否为常用设备列表 0 常用列表 1 非常用列表
        equipmentData:[],//设备列表
        modeSwitch:'1',//模式开关，0 开 1 关
        modeWidth:'',//模式开关，0 开 1 关
        modeList:[],//模式列表 {name:'',icon:'',index:''}
        modeIndex:'',//当前开启模式索引
        deviceType:{//设备类型选择
            isShow:false,//设备分类开关 状态  true显示 false隐藏
            allType:[
                {type:'all',name:'全部',id:'0'}
            ],//所有分类
            nowType:'all'//当前分类
        }
    },
    //加载设备类型种类
    loadDeviceType:function(data){
        var that = this;
        var clientCode = data.clientCode;
            wx.request({
                url: app.globalData.globalApi + "bisDevice/queryAllEquipmentType?clientCode=" + clientCode,
                data: {},
                dataType: "json",
                method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
                header: {
                    "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                    'experience-Pattern': app.globalData.experiencePattern// 是否为体验用户
                },
                success: function (res) {
                    var data = res.data;
                    if(data.code == 0 && data.data!=='' && data.data!== null){
                        var length = data.data.length;
                        var allType = [{type:'all',name:'全部',id:'0'}];
                        for (var i=0;i<length;i++){
                            allType.push({type:data.data[i].deviceTypeCode,name:data.data[i].name,id:i+1});
                        }
                        that.setData({
                            'deviceType.allType':allType
                        });
                    }else{
                        that.setData({
                            'deviceType.allType':[]//清空设备类型
                        });
                    }
                }
            });
    },
    tabDeviceType:function(e){//切换分类
        var that = this;
        var nowType = e.currentTarget.dataset.typecode;//获取当前选中设备分类
        that.setData({
            'deviceType.nowType':nowType,
            'deviceType.isShow':false
        });
        console.log(e)
    },
    showDeviceType:function(){//点击显示切换设备类型
        var that=this;
        that.setData({
            'deviceType.isShow':true
        })
    },
    //点击选择类型
    clickPerson:function(){
        var selectPerson = this.data.selectPerson;
        if(selectPerson == true){
            this.setData({
                selectArea:true,
                selectPerson:false,
            })
        }else{
            this.setData({
                selectArea:false,
                selectPerson:true,
            })
        }
    } ,
    //点击切换
    mySelect:function(e){
        const data = e.target.dataset;
        console.log(e);
        this.setData({
            firstPerson:{
                areaName:data.areaname,
                clientCode:data.clientcode,
                roleCode:data.rolecode
            },
            selectPerson:true,
            selectArea:false,
            equipmentCurrentIndex:'0'//切换大区域时候，默认加载当前区域下常用设备
        });
        this.equipmentMenuFuc(this.data.firstPerson)
    },
    equipmentMenuFuc:function(data){
        var that = this;
        var clientCode = data.clientCode;
        wx.request({
            url: app.globalData.globalApi+"home/getUserAccessBisArea/"+clientCode,
            data: {
                // clientCode:data.clientCode,
                // roleCode:data.roleCode
            },
            dataType:"json",
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session":app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                const data = res.data.data;
                const length = data.length;
                const baseArea =that.data.baseArea;
                const areaData = [];
                console.log(data);
                if(res.data.code ==0 && data && data !==''){
                    for(let i=0;i<length;i++){//组装区域数据
                        areaData.push({areaName:data[i].areaName,iconName:that.selectIco(data[i].areaIcon),areaCode:data[i].areaCode,dataIndex:i+1})
                    }
                    const newareaData = baseArea.concat(areaData);
                    that.setData({
                        equipmentArea:newareaData,
                    });
                    // that.baseEquipmentFun(clientCode);//默认加载常用设备-传入当前clientCode
                    var index = that.data.equipmentCurrentIndex;
                    if(index == '0'){
                        that.baseEquipmentFun(clientCode);
                    }else{
                        var areaCode = that.data.equipmentArea[index].areaCode;//获取选中区域areaCode
                        that.EquipmentFun(clientCode,areaCode);//设备列表
                        that.videoFuc(clientCode,areaCode);//是否存在视频
                    }
                }
            }
        });
    },
    navbarTap: function (e) {//tab切换
        var that = this;
        var index = e.currentTarget.dataset.index;
        //点击区域菜单-请求对应设备列表数据/判断是否为常用
        var clientCode = that.data.firstPerson.clientCode;//获取clientCode
        if(index!==that.data.equipmentCurrentIndex){//判断是否点击的当前高亮的区域
            if(index == '0'){
                that.baseEquipmentFun(clientCode);
            }else{
                var areaCode = that.data.equipmentArea[index].areaCode;//获取选中区域areaCode
                that.EquipmentFun(clientCode,areaCode);//设备列表
                that.videoFuc(clientCode,areaCode);//是否存在视频

            }
        }
        that.setData({
            equipmentCurrentIndex:index,
            // TypeItem : that.data.navbar[that.data.currentTab]
        });
    },
    baseEquipmentFun:function(data){//常用设备
        var that=this;
        var clientCode = data;
        //显示加载
        wx.showLoading({title:'加载中'});
        wx.request({
            url: app.globalData.globalApi+"home/loadMostCommon/"+clientCode,
            data: {
            },
            dataType:"json",
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session":app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                const data = res.data.data;
                console.log(data);
                //隐藏加载
                wx.hideLoading({title:'加载中'});//
                if(res.data.code ===0 && data.length !== 0) {//数据是否存在
                    const length = data.length;
                    const equipmentData = new Array();//更新设备列表
                    for(let i=0;i<length;i++){//组装设备数据
                        equipmentData.push({type:data[i].deviceTypeCode,name:data[i].name,bigArea:data[i].clientName,thisArea:data[i].areaName,name:data[i].name,equipmentIco:that.selectIco(data[i].icon),smallIco:[],switchStatic:'',switchCode:[],spotId:'',urlName:that.urlFuc(data[i].deviceTypeCode),deviceId:data[i].deviceId});
                        console.log(equipmentData)
                        for(let f=0;f<data[i].bisSpotDto.length;f++){
                            var k = f;
                            var spotType = data[i].bisSpotDto[k].spotType; //是否为开关选项
                            if(spotType=='C1'){
                                equipmentData[i].switchStatic=data[i].bisSpotDto[k].spotValue;
                                equipmentData[i].spotId=data[i].bisSpotDto[k].spotId;
                                for(let j=0;j<2;j++) {
                                    var itemValue = data[i].bisSpotDto[k].bisSpotItems[j].itemValue;
                                    if(data[i].bisSpotDto[k].bisSpotItems[j]){
                                        equipmentData[i].switchCode[itemValue]=data[i].bisSpotDto[k].bisSpotItems[j].spotItemId;
                                    }

                                }
                            }else{
                                equipmentData[i].smallIco.push({ico:that.selectIco(data[i].bisSpotDto[k].icon)});//组装小图标 过滤第一个图标（开关图标）
                            }
                        }
                    }
                    console.log(equipmentData);
                    that.setData({
                        isBase: '0',//常用列表
                        equipmentData: equipmentData,
                        noData:'0'
                    });
                }else if(res.data.code ===0 && data.length === 0){
                    that.setData({
                        isBase: '0',//常用列表
                        equipmentData:'',
                        noData:'1'
                    })
                }
            }
        });
    },
    EquipmentFun:function(clientCode,areaCode){//非常用设备-即对应区域设备
        var that = this;
        //显示加载
        wx.showLoading({title:'加载中'});
        wx.request({
            url: app.globalData.globalApi+"home/equipmentShow",
            data: {
                clientCode:clientCode,
                areaCode:areaCode
            },
            dataType:"json",
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session":app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                const data = res.data.data;
                console.log(data);
                //隐藏加载
                wx.hideLoading({title:'加载中'});//
                if(res.data.code ===0 && data.length !== 0){//数据是否存在
                    const length = data.length;
                    const equipmentData= new Array();//更新设备列表
                    for(let i=0;i<length;i++){//组装设备数据
                        equipmentData.push({type:data[i].deviceTypeCode,name:data[i].name,bigArea:data[i].clientName,thisArea:data[i].areaName,name:data[i].name,equipmentIco:that.selectIco(data[i].icon),smallIco:[],switchStatic:'',switchCode:[],spotId:'',urlName:that.urlFuc(data[i].deviceTypeCode),deviceId:data[i].deviceId});
                        console.log(equipmentData)
                        for(let f=0;f<data[i].bisSpotDto.length;f++){
                            var k = f;
                            var spotType = data[i].bisSpotDto[k].spotType; //是否为开关选项
                            if(spotType=='C1'){
                                equipmentData[i].switchStatic=data[i].bisSpotDto[k].spotValue;
                                equipmentData[i].spotId=data[i].bisSpotDto[k].spotId;
                                for(let j=0;j<2;j++) {
                                    var itemValue = data[i].bisSpotDto[k].bisSpotItems[j].itemValue;
                                    if(data[i].bisSpotDto[k].bisSpotItems.length !== 0){
                                        equipmentData[i].switchCode[itemValue]=data[i].bisSpotDto[k].bisSpotItems[j].spotItemId;
                                    }

                                }
                            }else{
                                equipmentData[i].smallIco.push({ico:that.selectIco(data[i].bisSpotDto[k].icon)});//组装小图标 过滤第一个图标（开关图标）
                            }
                        }
                    }
                    console.log(equipmentData);
                    that.setData({
                        isBase:'1',//非常用列表
                        equipmentData:equipmentData,
                        noData:'0'
                    });
                }else if(res.data.code ===0 && data.length === 0){
                    that.setData({
                        isBase: '1',//常用列表
                        equipmentData:'',
                        noData:'1',
                    })
                }
            }
        });
        //非常用设备时 加载情景模式
        that.modeLoadFuc(clientCode,areaCode);
    },
    videoFuc:function(clientCode,areaCode){//判断监控视频是否存在 有则调用
        var that = this;
        wx.request({
            url: app.globalData.globalApi+"home/getVedioDeviceAddress?client_code=" + clientCode + "&area_code=" + areaCode,
            data: {
            },
            dataType:"json",
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session":app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success:function (res) {
                var data = res.data;
                console.log(data)
                if(data.code == 0 && data.data !=='' && data.data !==null){//是否存在监控
                    console.log('222');
                    that.setData({//显示监控
                        topBlock:'0',
                        videoSrc:data.data
                    })
                }else{
                    that.setData({//显示天气
                        topBlock:'1',
                        videoSrc:''
                    })
                }
            }
        })
    },
    switchFun:function(e) {//设备开关 （小图标）
        var that = this;
        console.log(e);
        var switchValue = e.currentTarget.dataset.switchvalue;
        var index = e.currentTarget.dataset.index;
        var equipmentData = that.data.equipmentData;//取出设备数据
        var spotItemId = '';//传给后端状态码
        var spotId = equipmentData[index].spotId;
        var state = '';
        if (switchValue == '0') {
            // equipmentData[index].switchStatic = "1";
            state ='1';
            spotItemId = equipmentData[index].switchCode[1]
        } else {
            // equipmentData[index].switchStatic = "0";

            state ='0';
            spotItemId = equipmentData[index].switchCode[0]
        }
        that.switchResFuc(spotItemId, spotId,index,state)
    },
    switchResFuc:function(spotItemId,spotId,index,state){//请求关闭或开启当前设备
        var that = this;
        wx.showLoading();//
        wx.request({
            url: app.globalData.globalApi+"home/operationEquipment/"+spotId+"/"+spotItemId,
            data: {
            },
            dataType:"json",
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session":app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success:function(res){
                // console.log(res)
                var data = res.data;
                var switchKey = "equipmentData["+index+"].switchStatic";//修改开关状态
                if(data.code==0 && data.data!==''){
                    that.setData({
                        [switchKey]:state
                    })
                }
                wx.hideLoading();//
            }
        })
    },
    modeSwitchFuc:function(){//模式显隐开关
        var that=this;
        var state = that.data.modeSwitch;
        if (state=='0'){
            state = '1'
        } else{
            state = '0'
        }
        that.setData({
            modeSwitch:state,
        })
    },
    modeCellFuc:function (e) {//模式选择后触发
        var that =this;
        var clientCode = that.data.firstPerson.clientCode;
        var index = e.currentTarget.dataset.index;
        var deviceCode = e.currentTarget.dataset.devicecode;
        var spotItemId = e.currentTarget.dataset.spotitemid;
        wx.request({
            url: app.globalData.globalApi + "bisDevice/operationContextualModelByArea/"+deviceCode+"/"+clientCode+"/"+spotItemId,
            data: {},
            method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern': app.globalData.experiencePattern// 是否为体验用户
            },
            success: function (res) {
                if(res.data.code == 0){
                    that.setData({
                        modeSwitch:'1',//关闭选择框
                        modeIndex:index
                    })
                }

            }
        });
    },
    selectIco:function(id){//根据传入图标代码获取图标名
        var that = this;
        var num = "";//截取图标编码后三位
        var baseIco = that.data.baseIco[0];
        var thisIcoVal = "";
        var className = "";
        if (id){
            num = id.substring(id.length-4,id.length-1);
            for( let i in baseIco) {
                thisIcoVal = baseIco[i];
                if (thisIcoVal == num){//匹配到图标时跳出
                    className = i;
                    break;
                }
            }
        }
        return className;
    },
    urlFuc:function(type){
        var that = this;
        var urlName = '';
        if(type == 'AC'){
            urlName = 'controlAirconditioning'
        }else if(type == 'DC'){
            urlName = 'contralAccess'
        }else if(type == 'WD'){
            urlName = 'controlCurtain'
        }
        return urlName;
    },
    goDetailFuc:function (e) {
        var that = this;
        var deviceId = e.currentTarget.dataset.deviceid;
        var urlName = e.currentTarget.dataset.urlname;
        if (urlName) {
            wx.navigateTo({
                url: '../' + urlName + '/' + urlName + '?deviceId=' + deviceId   //跳转详情页
            });
        }
    },
    //设备列表组装函数
    equipMentList:function(data){
        var that = this;
        const length = data.length;
        const equipmentData= new Array();//更新设备列表
        for(let i=0;i<length;i++){//组装设备数据
            equipmentData.push({type:data[i].deviceTypeCode,name:data[i].name,bigArea:data[i].clientName,thisArea:data[i].areaName,name:data[i].name,equipmentIco:that.selectIco(data[i].icon),smallIco:[],switchStatic:'',switchCode:[],spotId:'',urlName:that.urlFuc(data[i].deviceTypeCode),deviceId:data[i].deviceId});
            console.log(equipmentData)
            for(let f=0;f<data[i].bisSpotDto.length;f++){
                var k = f;
                var spotType = data[i].bisSpotDto[k].spotType; //是否为开关选项
                if(spotType=='C1'){
                    equipmentData[i].switchStatic=data[i].bisSpotDto[k].spotValue;
                    equipmentData[i].spotId=data[i].bisSpotDto[k].spotId;
                    for(let j=0;j<2;j++) {
                        var itemValue = data[i].bisSpotDto[k].bisSpotItems[j].itemValue;
                        if(data[i].bisSpotDto[k].bisSpotItems[j]){
                            equipmentData[i].switchCode[itemValue]=data[i].bisSpotDto[k].bisSpotItems[j].spotItemId;
                        }
                    }
                }else{
                    equipmentData[i].smallIco.push({ico:that.selectIco(data[i].bisSpotDto[k].icon)});//组装小图标 过滤第一个图标（开关图标）
                }
            }
        }
        return equipmentData;
    },
    //websocket函数
    socketFuc:function(){
        var that = this;
        var socketTask = wx.connectSocket({
            url: app.globalData.socketUrl,
            success:function (res) {
                console.log(res)
            }
        });
        console.log(socketTask)
        that.setData({
            task:socketTask
        });
        // 监听链接成功
        socketTask.onOpen(function(res) {
            console.log(res);
            socketTask.send({
                data: app.globalData.tokenSession,
                success: res => {
                    console.log(res+'发送成功')
                },
                fail: err=> {
                    console.log(err+'失败')
                }

            });
        });
        // 接收更新数据
        socketTask.onMessage(function(data) {
            var objData = JSON.parse(data.data);
            console.log(objData);
            var clientData = objData.clientData;
                var data_type = objData.data_type;
                // var data_type = '222'
                if(data_type=='device_change'){//如果传入数据为更新设备
                    var length = clientData.length;
                    var deviceId = '';
                    var nowDeviceId = '';
                    var lengthN = that.data.equipmentData.length;
                    var dataArray = [];
                    var updateData = '';
                    if(lengthN!==0){//当前设备列表存在数据时候
                        for(let i=0;i<length;i++){
                            deviceId = clientData[i].deviceId;//获取设备id
                            for(let j=0;j<lengthN;j++){
                                nowDeviceId = that.data.equipmentData[j].deviceId;
                                if (deviceId == nowDeviceId){//匹配对应数据位置
                                    dataArray[0]=clientData[i];//写入数组
                                    updateData = that.equipMentList(dataArray);//组装为设备列表显示数据格式
                                    that.data.equipmentData[j] = updateData[0];//写入单条数据到列表 更新页面
                                    that.setData({//重设设备列表数据
                                        equipmentData:that.data.equipmentData,
                                    })
                                }
                            }
                        }
                    }

                }else{//权限有变更 给出提示后跳转首页
                    that.show('权限有变更','failed','','../controlIndex/controlIndex');//弹出提示信息
                    that.onShow();
                }

        });
        //连接失败
        socketTask.onError(function() {
            console.log('websocket连接失败！');
        });
    },
    onLoad:function(options){
        // 调用应用实例的方法获取全局数据
        let app = getApp();
        // toast组件实例
        new app.ToastPannel();

    },
    onReady:function(){
        // 页面渲染完成
    },
    onShow:function(){
        // 页面显示
        var that = this;

        // 页面初始化 options为页面跳转所带来的参数
        // const that=this;
        that.weatherFuc();//加载天气
        wx.request({//加载默认数据
            url: app.globalData.globalApi+"home/localHouseholdArea?",
            data: {
                isLocation:false
            },
            dataType:"json",
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session":app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                const data = res.data.data;
                const length = data.length;
                const areaData = new Array();//更新区域
                console.log(data);
                console.log(length);
                if(res.data.code === 0 && length !== 0){
                    for(let i=0;i<length;i++){//组装区域数据
                        areaData.push({areaName:data[i].name, clientCode:data[i].clientCode, roleCode:data[i].roleCode})
                    }
                    console.log(areaData);
                    that.setData({
                        firstPerson:{
                            areaName:data[0].name,
                            clientCode:data[0].clientCode,
                            roleCode:data[0].roleCode
                        },
                        areaData:areaData,
                        noData:'0'
                    });
                    app.globalData.hasAreaNum = length;//全局 设置当前用户所有区域数量（其他页面引用此全局变量）
                    that.equipmentMenuFuc(that.data.firstPerson);//加载左侧设备列表
                    that.loadDeviceType(that.data.firstPerson);//加载所有分类
                }else if(res.data.code === 0 && length === 0){//请求成功 但没有区域
                    var newequipmentArea = that.data.baseArea;
                    that.setData({
                        firstPerson:{
                            areaName:'暂无',
                            clientCode:'',
                            roleCode:''
                        },
                        areaData:areaData,
                        noData:'1',
                        equipmentArea:newequipmentArea,//如果没有大区数据 左侧只显示默认常用
                        equipmentData:'',//清空设备列表
                        modeSwitch:'1',//关闭模式层
                        modeList:[],//清空模式数据
                        'deviceType.allType':[]//清空设备类型
                    });
                }

            }
        });

        that.socketFuc();//连接websocket
    },
    onHide:function(){
        // 页面隐藏
        var that = this;
        that.data.task.close();//关闭当前页websocket
    },
    onUnload:function(){
        // 页面关闭或返回
        var that = this;
        // that.data.task.close();//关闭当前页websocket
    },
    modeLoadFuc:function(clientCode,areaCode){//加载模式列表
        var that = this;
        var width = (wx.getSystemInfoSync().windowWidth - 120)+'px';
        that.setData({
           modeWidth:width,
            modeSwitch:'1',//关闭模式层
            modeList:[],//清空模式层
        });
        wx.request({
            url: app.globalData.globalApi + "bisDevice/queryAllContextualModelByArea",
            data: {
                client_code:clientCode,
                area_code:areaCode
            },
            method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern': app.globalData.experiencePattern// 是否为体验用户
            },
            success: function (res) {
                const data = res.data.data;
                const modeData = [];
                if(res.data.code === 0 && data!==null){
                    const length = data.length;
                    const listData = data.bisSpotDto[0].bisSpotItems;
                    for(let i=0;i<listData.length;i++){
                        modeData.push({name:listData[i].itemName,icon:that.selectIco(listData[i].itemIcon),deviceCode:listData[i].deviceCode,spotItemId:listData[i].spotItemId,index:listData[i].itemValue})
                    }
                    that.setData({
                        modeList:modeData,
                        modeIndex:data.bisSpotDto[0].spotValue,
                    })
                }else{

                }
            }
        });
    },
    weatherFuc:function(){//获取天气信息
        const that = this;
        const ipaddr = "";
        wx.request({//加载天气
            url: app.globalData.globalApi+"home/getWeatherInfo",
            data: {
                ipaddr:ipaddr
            },
            method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session":app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){//重设天气
                const weatherList={
                    "00":"晴",
                    "01":"多云",
                    "02":"阴",
                    "03":"阵雨",
                    "04":"雷阵雨",
                    "05":"雷阵雨并伴有冰雹",
                    "06":"雨夹雪",
                    "07":"小雨",
                    "08":"中雨",
                    "09":"大雨",
                    "10":"暴雨",
                    "11":"大暴雨",
                    "12":"特大暴雨",
                    "13":"阵雪",
                    "14":"小雪",
                    "15":"中雪",
                    "16":"大雪",
                    "17":"暴雪",
                    "18":"雾",
                    "19":"冻雨",
                    "20":"沙尘暴",
                    "21":"小雨-中雨",
                    "22":"中雨-大雨",
                    "23":"大雨-暴雨",
                    "24":"暴雨-大暴雨",
                    "25":"大暴雨-特大暴雨",
                    "26":"小雪-中雪",
                    "27":"中雪-大雪",
                    "28":"大雪-暴雪",
                    "29":"浮尘",
                    "30":"扬沙",
                    "31":"强沙尘暴",
                    "32":"飑",
                    "33":"龙卷风",
                    "34":"弱高吹雪",
                    "35":"轻雾",
                    "53":"霾"
                };
                const data = JSON.parse(res.data.data);
                const weatherVal = data[0].weather;
                var weatherNum ="";
                for(var i in weatherList){//匹配当前天气对应图表名称
                    if(weatherList[i]==weatherVal){
                        weatherNum = i;
                    }
                }
                if(weatherVal> 0){//判断天气对应图标 -- 暂不处理
                }
                that.setData({
                    weather:{
                        province:data[0].province,//省
                        city:data[0].city,//市
                        tempNum:data[0].temperature,//温度
                        weatherVal:data[0].weather,//天气
                        weatherIco:'5',//天气图标
                        winddirection:data[0].winddirection,//风向
                        windpower:data[0].windpower //风级
                    }
                })
            }
        })
    },
    closeWinFuc:function () {
        var that = this;
        that.setData({
            'deviceType.isShow':false,
        })
    }
});
