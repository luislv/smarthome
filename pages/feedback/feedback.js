//logs.js
const app = getApp()
Page({
    data: {
        feedback:{
            fbTitle:'',//标题
            toUserMobile:'',//手机号
            fbContent:''//提交内容
        }
    },
    onLoad: function () {
        // 调用应用实例的方法获取全局数据
        let app = getApp();
        // toast组件实例
        new app.ToastPannel();
        var that = this;
    },
    checkInp:function(e){  //失去焦点时 数据写入对应字段
            var that = this;
            var  title = e.detail.value;
            var typeName = 'feedback.'+e.currentTarget.dataset.typename;
            that.setData({
                [typeName]:title
            });
    },
    commitFuc:function () {//提交
        var that = this;
        var data = that.data.feedback;
        var isPhone = !(/^1[34578][0-9]\d{4,8}$/.test(data.toUserMobile))
        //校验
        if(data.fbTitle === ''){
            that.show('标题不能为空！','failed','');
            return false;
        }else if(data.toUserMobile === ''){
            that.show('手机号不能为空！','failed','');
            return false;
        }else if(isPhone){
            that.show('手机号格式不正确！','failed','');
            return false;
        }else if(data.fbContent === ''){
            that.show('内容不能为空！！','failed','');
            return false;
        }
        //显示等待窗
        wx.showLoading();
        wx.request({//大区列表接口
            url: app.globalData.globalApi + "home/insertFeedback",
            data: data,
            dataType: "json",
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern': app.globalData.experiencePattern// 是否为体验用户
            },
            success: function (res) {
                //隐藏等待窗
                wx.hideLoading();
                that.show('提交成功','success','','../controlIndex/controlIndex');//提交成功
            }
        });

    }
});
