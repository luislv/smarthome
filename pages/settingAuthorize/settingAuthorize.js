//index.js
//获取应用实例
const app = getApp()
Page({
    data: {
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        hasAreaNum:"",//区域数
        authData:""// [{clientNameRename: "XXの别墅", userCount: 2, clientCode: "001"}]
    },
    onLoad: function () {
        var that = this;
        if (app.globalData.userInfo) {
            this.setData({
                userInfo: app.globalData.userInfo,
                hasUserInfo: true
            })
        } else if (this.data.canIUse){
            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
            // 所以此处加入 callback 以防止这种情况
            app.userInfoReadyCallback = res => {
                this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                })
            }
        } else {
            // 在没有 open-type=getUserInfo 版本的兼容处理
            wx.getUserInfo({
                success: res => {
                    app.globalData.userInfo = res.userInfo
                    this.setData({
                        userInfo: res.userInfo,
                        hasUserInfo: true
                    })
                }
            })
        }
        this.setData({
            hasAreaNum:app.globalData.hasAreaNum //设置当前用户所有区域数量
        });

        //显示加载中
        wx.showLoading({title:'加载中'});//
        //获取区域列表
        wx.request({
            url: app.globalData.globalApi+"bisUserBindController/authorizedIndex",//获取设置用户配置接口
            data:{
            },
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            dataType:'json',
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                //隐藏加载中
                wx.hideLoading({title:'加载中'});//
                if(res.data.success == true){
                    const data = res.data.data.bisClientUserRelationDtoList;
                    const length = data.length;
                    const authData = new Array();//
                    for(let i=0;i<length;i++){//组装区域数据
                        authData.push({clientNameRename:data[i].clientNameRename,userCount:data[i].userCount,clientCode:data[i].clientCode});
                    }
                    console.log(authData);
                    that.setData({
                        authData:authData
                    })
                }else{

                }
            }
        })
    },
    getUserInfo: function(e) {
        console.log(e);
        app.globalData.userInfo = e.detail.userInfo;
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true
        })
    }
});
