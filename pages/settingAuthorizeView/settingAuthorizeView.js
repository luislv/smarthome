//index.js
//获取应用实例
const app = getApp()
Page({
    data: {
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        hasAreaNum:"",//区域数
        authData:""// [{userName: "吕俊", count: 2, clientCode: "001",userId:"1008612"}]
    },
    onLoad: function (options) {
        var that = this;
        this.setData({
            hasAreaNum:app.globalData.hasAreaNum //设置当前用户所有区域数量
        });
        if (app.globalData.userInfo) {
            this.setData({
                userInfo: app.globalData.userInfo,
                hasUserInfo: true
            })
        } else if (this.data.canIUse){
            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
            // 所以此处加入 callback 以防止这种情况
            app.userInfoReadyCallback = res => {
                this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                })
            }
        } else {
            // 在没有 open-type=getUserInfo 版本的兼容处理
            wx.getUserInfo({
                success: res => {
                    app.globalData.userInfo = res.userInfo
                    this.setData({
                        userInfo: res.userInfo,
                        hasUserInfo: true
                    })
                }
            })
        }
        var clientCode = options.clientCode;//接收传入clientCode参数
        this.setData({
            clientCode:clientCode
        });
    },
    onShow:function(){//使用onshow 不刷新页面 更新数据
        var that = this;
        var clientCode = that.data.clientCode;
        //显示加载中
        wx.showLoading({title:'加载中'});//
        wx.request({
            url: app.globalData.globalApi+"bisUserAuthorizationRecord/queryBisClientUserRelationByUserIdAndClientCode",//加载用户列表
            data:{
                clientCode:clientCode
            },
            method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            dataType:'json',
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                //隐藏加载中
                wx.hideLoading({title:'加载中'});//
                if(res.data.success == true){
                    const data = res.data.data;
                    const length = data.length;
                    const authData = new Array();//
                    for(let i=0;i<length;i++){//组装区域数据
                        if(data[i].userId=='' || data[i].userId==null ||data[i].userId==undefined){
                            authData.push({userName:data[i].userPhone, count:data[i].count, clientCode:data[i].clientCode,userPhone:data[i].userPhone,forbidden:data[i].status});
                        }else{
                            authData.push({userName:data[i].userName, count:data[i].count, clientCode:data[i].clientCode,userId:data[i].userId,forbidden:data[i].status});
                        }
                    }
                    console.log(authData);
                    that.setData({
                        authData:authData
                    })
                }else{

                }
            }
        })
    },
    getUserInfo: function(e) {
        console.log(e)
        app.globalData.userInfo = e.detail.userInfo
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true
        })
    },
    goDetailFuc:function (e) {//跳转详情页
        var that = this;
        var data = e.currentTarget.dataset;
        var urlData = '';
        console.log(data.userphone);
        console.log(data.userid);
        if(!data.userid||data.userid ==''||data.userid == undefined){
            urlData = '?clientCode='+data.clientcode+'&userPhone='+data.userphone+'&forbidden='+data.forbidden
        }else{
            urlData = '?clientCode='+data.clientcode+'&userId='+data.userid+'&forbidden='+data.forbidden
        }
        console.log(urlData);
        wx.navigateTo({
            url: '../settingAuthorizeDetail/settingAuthorizeDetail'+urlData   //跳转详情页
        });
    },
    goAddEditFuc:function(){
        var that = this;
        var clientCode = that.data.clientCode;
        app.globalData.authorizeData = true; //设置为ture 判断为进入新增页面
        wx.navigateTo({
            url: '../settingAuthorizeAddEdit/settingAuthorizeAddEdit?clientCode='+clientCode   //新增授权
        });
    }

});
