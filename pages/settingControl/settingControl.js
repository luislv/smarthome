//index.js
//获取应用实例
const app = getApp()
Page({
    data: {
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        isAdmin:'',
        hasAreaNum:"",
        checkboxItems: [
            // {name: '收到提醒时是否为推送', value: '0', checked: true},
            // {name: '区域是否展示监控画面（当有监控时）', value: '1',checked:false},
            // {name: '自动根据定位切换区域（需开启地理位置）', value: '2',checked:false}
        ],
    },
    onLoad: function () {
        var that = this;
        //用户信息判断
        if (app.globalData.userInfo) {
            this.setData({
                userInfo: app.globalData.userInfo,
                hasUserInfo: true
            })
        } else if (this.data.canIUse){
            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
            // 所以此处加入 callback 以防止这种情况
            app.userInfoReadyCallback = res => {
                this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                })
            }
        } else {
            // 在没有 open-type=getUserInfo 版本的兼容处理
            wx.getUserInfo({
                success: res => {
                    app.globalData.userInfo = res.userInfo
                    this.setData({
                        userInfo: res.userInfo,
                        hasUserInfo: true
                    })
                }
            })
        }
        this.setData({
            hasAreaNum:app.globalData.hasAreaNum //设置当前用户所有区域数量
        });
        //获取设置用户配置
        wx.request({
            url: app.globalData.globalApi+"bisUserBindController/toConfigIndex",//获取设置用户配置接口
            data:{
            },
            method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            dataType:'json',
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                const data = res.data.data;
                const length = data.length;
                const authData = new Array();//更新区域
                for(let i=0;i<length;i++){//组装区域数据
                    if(data[i].configValue == 0){
                        authData.push({name:data[i].configDesc, value:data[i].id, checked:true})
                    }else{
                        authData.push({name:data[i].configDesc, value:data[i].id,checked:false})
                    }
                }
                that.setData({
                    checkboxItems:authData
                })
            }
        })


    },
    getUserInfo: function(e) {
        console.log(e)
        app.globalData.userInfo = e.detail.userInfo
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true
        })
    },
    checkboxChange: function (e) {
        var that = this;
        // console.log('checkbox发生change事件，携带value值为：', e);
        var checkboxItems = that.copyArr(that.data.checkboxItems);
        var checkboxItemsNew = that.data.checkboxItems, values = e.detail.value;
        for (var i = 0, lenI = checkboxItemsNew.length; i < lenI; ++i) {
            checkboxItemsNew[i].checked = false;
            for (var j = 0, lenJ = values.length; j < lenJ; ++j) {
                if(checkboxItemsNew[i].value == values[j]){
                    checkboxItemsNew[i].checked = true;
                    break;
                }
            }
        }
        var configId = "";//开关id
        var configValue = "";//开关值
        for (var i = 0, lenI = checkboxItemsNew.length; i < lenI; ++i) {
            if (checkboxItemsNew[i].checked !== checkboxItems[i]){
                configId = checkboxItemsNew[i].value;//绑定回传id
                if(checkboxItemsNew[i].checked == true){
                        configValue="0"
                }else{
                    configValue="1"
                }
            }
        }
        //显示操作中
        wx.showLoading();//
        wx.request({//存储开关数据
            url: app.globalData.globalApi+"bisUserBindController/updateConfigInfo",//更新设置用户配置接口
            data:{
                config_id:configId,
                config_value:configValue
            },
            method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
            header: {
                "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                'experience-Pattern':app.globalData.experiencePattern// 是否为体验用户
            },
            success: function(res){
                var data = res.data;
                //隐藏操作中
                wx.hideLoading();//
                if(data.success == true) {
                    that.setData({//更新开关数据
                        checkboxItems: checkboxItemsNew
                    });
                }else{
                    app.openAlert(data.msg);
                }
            },
            fail:function (res) {

            }
        })
    },
    copyArr:function (arr) {//copy开关状态
    let res = []
    for (let i = 0; i < arr.length; i++) {
        res.push(arr[i].checked)
    }
    return res
    },
    experienceFuc:function(){//虚拟体验 app.globalData.experiencePattern值为空时非虚拟体验  值为EXPERIENCE_PATTERN 为虚拟体验
        var that = this;
        var experienceStatus = app.globalData.experiencePattern;
        if (experienceStatus == ''){
            app.globalData.experiencePattern = 'EXPERIENCE_PATTERN';
            app.globalData.experiencePatternValue = '关闭虚拟体验';
        }else{
            app.globalData.experiencePattern = '';
            app.globalData.experiencePatternValue = '虚拟体验';
        }
        wx.redirectTo({
            url: '../controlIndex/controlIndex'
        })
    },
    onShow:function () {
        var that = this;
        that.setData({//重新渲染虚拟体验值
            experiencePatternValue:app.globalData.experiencePatternValue,
            isAdmin:app.globalData.isAdmin
        })
    },
    toSettingAuthorize:function () {
        wx.navigateTo({
            url: '../settingAuthorize/settingAuthorize'
        })
    },
    toSettingMessage:function () {
        wx.navigateTo({
            url: '../settingMessage/settingMessage'
        })
    }
});
