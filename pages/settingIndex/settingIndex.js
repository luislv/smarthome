//index.js
//获取应用实例
const app = getApp()
Page({
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
      hasAreaNum:"",
      experiencePatternValue:app.globalData.experiencePatternValue,
      isAdmin:''
  },
  onLoad: function () {
    var that = this;
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse){
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo;
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
    this.setData({
        hasAreaNum:app.globalData.hasAreaNum //设置当前用户所有区域数量
    })
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
    experienceFuc:function(){//虚拟体验 app.globalData.experiencePattern值为空时非虚拟体验  值为EXPERIENCE_PATTERN 为虚拟体验
        var that = this;
        var experienceStatus = app.globalData.experiencePattern;
        if (experienceStatus == ''){
            app.globalData.experiencePattern = 'EXPERIENCE_PATTERN';
            app.globalData.experiencePatternValue = '关闭虚拟体验';
        }else{
            app.globalData.experiencePattern = '';
            app.globalData.experiencePatternValue = '虚拟体验';
        }
        wx.navigateBack({//返回
            delta:1
        })
    },
    onShow:function () {
        var that = this;
        that.setData({//重新渲染虚拟体验值
            experiencePatternValue:app.globalData.experiencePatternValue,
            isAdmin:app.globalData.isAdmin
        })
    },
    toSettingAuthorize:function () {
        wx.navigateTo({
            url: '../settingAuthorize/settingAuthorize'
        })
    },
    toSettingMessage:function () {
        wx.navigateTo({
            url: '../settingMessage/settingMessage'
        })
    }
});
