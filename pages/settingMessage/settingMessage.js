//index.js
//获取应用实例
const app = getApp()
Page({
    data: {
        motto: 'Hello World',
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        hasAreaNum:'',
        logArr:{
            currentPage:'1',
            pages:'',
            logData:[]
        },//安全日志消息列表
        height: '',
        res: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    },
    /**
     * 生命周期函数--监听页面加载
     */
    lower() {//滚动加载更多
        var that = this;
        var currentPage = that.data.logArr.currentPage + 1;
        var pages = that.data.logArr.pages;
        if (currentPage > pages){//判断是否已经是最后一页
            wx.showToast({ //如果全部加载完成了也弹一个框
                        title: '没有更多数据',
                        icon: 'success',
                        duration: 300
                    });
                    return false;
        }else{
            wx.request({
                url: app.globalData.globalApi + "home/getLogInfo",
                data:{
                    currentPage:currentPage,
                    pageSize:'10'
                },
                dataType:"json",
                method: 'get', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
                header: {
                    "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                    'experience-Pattern': app.globalData.experiencePattern// 是否为体验用户
                },
                success: function (res) {
                    console.log(res);
                    wx.showLoading({ //期间为了显示效果可以添加一个过度的弹出框提示“加载中”
                        title: '加载中',
                        icon: 'loading',
                    });
                    var data = res.data;
                    if (data.code==0 && data.data!==''){//存在数据时
                        var currentPage = data.data.current;//当前页数
                        var pages = data.data.pages;//所有页数
                        var length= data.data.records.length;
                        var result = that.data.logArr.logData;//当前页之前所有数据
                        var logData=[];//当前页返回数据组装数组
                        var resultData = []; //重新组装所有数据
                        for(var i=0;i<length;i++){
                            logData.push({title:data.data.records[i].title,messContent:data.data.records[i].messContent,time:that.secondToDate(data.data.records[i].createTime)})
                        }
                        resultData = result.concat(logData);
                        that.setData({//载入日志
                            logArr:{
                                currentPage:currentPage,
                                pages:pages,
                                logData:resultData
                            }
                        });
                        wx.hideLoading();//关闭加载窗
                    }
                }
            })
        }



    },
    onLoad: function () {
        var that = this;
        if (app.globalData.userInfo) {
            this.setData({
                userInfo: app.globalData.userInfo,
                hasUserInfo: true
            })
        } else if (this.data.canIUse){
            // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
            // 所以此处加入 callback 以防止这种情况
            app.userInfoReadyCallback = res => {
                this.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                })
            }
        } else {
            // 在没有 open-type=getUserInfo 版本的兼容处理
            wx.getUserInfo({
                success: res => {
                    app.globalData.userInfo = res.userInfo
                    this.setData({
                        userInfo: res.userInfo,
                        hasUserInfo: true
                    })
                }
            })
        }
        this.setData({
            hasAreaNum:app.globalData.hasAreaNum //设置当前用户所有区域数量
        });
        wx.getSystemInfo({
            success: (res) => {
                this.setData({
                    height: res.windowHeight
                })
            }
        });
        //加载日志
        wx.request({
                url: app.globalData.globalApi + "home/getLogInfo",
                data:{
                    currentPage:'1',
                    pageSize:'10'
                },
                dataType:"json",
                method: 'post', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
                header: {
                    "token-session": app.globalData.tokenSession, // 设置请求的header，写入返回token
                    'experience-Pattern': app.globalData.experiencePattern// 是否为体验用户
                },
                success: function (res) {
                    console.log(res);
                    var data = res.data;
                    if (data.code==0 && data.data!==''){
                        var currentPage = data.data.current;//当前页数
                        var pages = data.data.pages;//所有页数
                        var length= data.data.records.length;
                        var logData=[];//预设组装数据数组
                        for(var i=0;i<length;i++){
                            logData.push({title:data.data.records[i].title,messContent:data.data.records[i].messContent,time:that.secondToDate(data.data.records[i].createTime)})
                        }
                        that.setData({//载入日志
                            logArr:{
                                currentPage:currentPage,
                                pages:pages,
                                logData:logData
                            }
                        });
                        console.log(that.data.logArr)
                    }
                }
        })
    },
    getUserInfo: function(e) {
        console.log(e);
        app.globalData.userInfo = e.detail.userInfo;
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true
        })
    },
    secondToDate:function(nS){
        var time = (new Date(nS)).toLocaleString();
        return time;
    }
    });
